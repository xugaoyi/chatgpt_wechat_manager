import request from '@/utils/request'

// 查询品牌或者厂家信息列表
export function listInfo(query) {
  return request({
    url: '/jhc/info/list',
    method: 'get',
    params: query
  })
}

// 查询品牌或者厂家信息详细
export function getInfo(id) {
  return request({
    url: '/jhc/info/' + id,
    method: 'get'
  })
}

// 新增品牌或者厂家信息
export function addInfo(data) {
  return request({
    url: '/jhc/info',
    method: 'post',
    data: data
  })
}

// 修改品牌或者厂家信息
export function updateInfo(data) {
  return request({
    url: '/jhc/info',
    method: 'put',
    data: data
  })
}

// 删除品牌或者厂家信息
export function delInfo(id) {
  return request({
    url: '/jhc/info/' + id,
    method: 'delete'
  })
}



// 查询品牌或者厂家信息列表
export function listInfosys(query) {
  return request({
    url: '/jhc/info/sys/list',
    method: 'get',
    params: query
  })
}

// 查询品牌或者厂家信息详细
export function getInfosys(id) {
  return request({
    url: '/jhc/info/sys/' + id,
    method: 'get'
  })
}

// 新增品牌或者厂家信息
export function addInfosys(data) {
  return request({
    url: '/jhc/info/sys',
    method: 'post',
    data: data
  })
}

// 修改品牌或者厂家信息
export function updateInfosys(data) {
  return request({
    url: '/jhc/info/sys',
    method: 'put',
    data: data
  })
}

// 删除品牌或者厂家信息
export function delInfosys(id) {
  return request({
    url: '/jhc/info/sys/' + id,
    method: 'delete'
  })
}


