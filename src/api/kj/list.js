import request from '@/utils/request'

// 查询kj-建立平台列列表
export function listList(query) {
  return request({
    url: '/kj/list/list',
    method: 'get',
    params: query
  })
}

// 查询kj-建立平台列详细
export function getList(platformId) {
  return request({
    url: '/kj/list/' + platformId,
    method: 'get'
  })
}

// 新增kj-建立平台列
export function addList(data) {
  return request({
    url: '/kj/list',
    method: 'post',
    data: data
  })
}

// 修改kj-建立平台列
export function updateList(data) {
  return request({
    url: '/kj/list',
    method: 'put',
    data: data
  })
}

// 删除kj-建立平台列
export function delList(platformId) {
  return request({
    url: '/kj/list/' + platformId,
    method: 'delete'
  })
}



// 查询kj-建立平台列列表
export function listListsys(query) {
  return request({
    url: '/kj/list/sys/list',
    method: 'get',
    params: query
  })
}

// 查询kj-建立平台列详细
export function getListsys(platformId) {
  return request({
    url: '/kj/list/sys/' + platformId,
    method: 'get'
  })
}

// 新增kj-建立平台列
export function addListsys(data) {
  return request({
    url: '/kj/list/sys',
    method: 'post',
    data: data
  })
}

// 修改kj-建立平台列
export function updateListsys(data) {
  return request({
    url: '/kj/list/sys',
    method: 'put',
    data: data
  })
}

// 删除kj-建立平台列
export function delListsys(platformId) {
  return request({
    url: '/kj/list/sys/' + platformId,
    method: 'delete'
  })
}


