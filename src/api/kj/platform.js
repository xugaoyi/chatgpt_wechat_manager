import request from '@/utils/request'

// 查询用户创建的平台列表
export function listPlatform(query) {
  return request({
    url: '/kj/platform/list',
    method: 'get',
    params: query
  })
}

// 查询用户创建的平台详细
export function getPlatform(ctPlatformId) {
  return request({
    url: '/kj/platform/' + ctPlatformId,
    method: 'get'
  })
}

// 新增用户创建的平台
export function addPlatform(data) {
  return request({
    url: '/kj/platform',
    method: 'post',
    data: data
  })
}

// 修改用户创建的平台
export function updatePlatform(data) {
  return request({
    url: '/kj/platform',
    method: 'put',
    data: data
  })
}

// 删除用户创建的平台
export function delPlatform(ctPlatformId) {
  return request({
    url: '/kj/platform/' + ctPlatformId,
    method: 'delete'
  })
}



// 捡漏
export function lastMinute(params) {
  return request({
    url: '/kj/platform/lastMinute?ctPlatformId=' + params.ctPlatformId +"&rushPurchaseType="+params.rushPurchaseType+"&rushPurchaseNumber="+params.rushPurchaseNumber,
    method: 'post'
  })
}


// 捡漏停止
export function lastMinuteStop(ctPlatformId) {
  return request({
    url: '/kj/platform/lastMinuteStop?ctPlatformId=' + ctPlatformId,
    method: 'post'
  })
}


// 查询用户创建的平台列表
export function listPlatformsys(query) {
  return request({
    url: '/kj/platform/sys/list',
    method: 'get',
    params: query
  })
}

// 查询用户创建的平台详细
export function getPlatformsys(ctPlatformId) {
  return request({
    url: '/kj/platform/sys/' + ctPlatformId,
    method: 'get'
  })
}

// 新增用户创建的平台
export function addPlatformsys(data) {
  return request({
    url: '/kj/platform/sys',
    method: 'post',
    data: data
  })
}

// 修改用户创建的平台
export function updatePlatformsys(data) {
  return request({
    url: '/kj/platform/sys',
    method: 'put',
    data: data
  })
}

// 删除用户创建的平台
export function delPlatformsys(ctPlatformId) {
  return request({
    url: '/kj/platform/sys/' + ctPlatformId,
    method: 'delete'
  })
}


