import request from '@/utils/request'

// 查询建立用户附加列表
export function listOther(query) {
  return request({
    url: '/kj/other/list',
    method: 'get',
    params: query
  })
}

// 查询建立用户附加详细
export function getOther(userOtherId) {
  return request({
    url: '/kj/other/' + userOtherId,
    method: 'get'
  })
}

// 新增建立用户附加
export function addOther(data) {
  return request({
    url: '/kj/other',
    method: 'post',
    data: data
  })
}

// 修改建立用户附加
export function updateOther(data) {
  return request({
    url: '/kj/other',
    method: 'put',
    data: data
  })
}

// 删除建立用户附加
export function delOther(userOtherId) {
  return request({
    url: '/kj/other/' + userOtherId,
    method: 'delete'
  })
}



// 查询建立用户附加列表
export function listOthersys(query) {
  return request({
    url: '/kj/other/sys/list',
    method: 'get',
    params: query
  })
}

// 查询建立用户附加详细
export function getOthersys(userOtherId) {
  return request({
    url: '/kj/other/sys/' + userOtherId,
    method: 'get'
  })
}

// 新增建立用户附加
export function addOthersys(data) {
  return request({
    url: '/kj/other/sys',
    method: 'post',
    data: data
  })
}

// 修改建立用户附加
export function updateOthersys(data) {
  return request({
    url: '/kj/other/sys',
    method: 'put',
    data: data
  })
}

// 删除建立用户附加
export function delOthersys(userOtherId) {
  return request({
    url: '/kj/other/sys/' + userOtherId,
    method: 'delete'
  })
}


