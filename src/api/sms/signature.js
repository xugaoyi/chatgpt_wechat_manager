import request from '@/utils/request'

// 查询签名管理列表
export function listSignature(query) {
  return request({
    url: '/sms/signature/list',
    method: 'get',
    params: query
  })
}

// 查询签名管理列表
export function listSignatureTemp(query) {
  return request({
    url: '/sms/signature/list',
    method: 'get',
    params: query
  })
}


// 查询签名管理详细
export function getSignature(smsSignatureId) {
  return request({
    url: '/sms/signature/' + smsSignatureId,
    method: 'get'
  })
}

// 新增签名管理
export function addSignature(data) {
  return request({
    url: '/sms/signature',
    method: 'post',
    data: data
  })
}

// 修改签名管理
export function updateSignature(data) {
  return request({
    url: '/sms/signature',
    method: 'put',
    data: data
  })
}

// 删除签名管理
export function delSignature(smsSignatureId) {
  return request({
    url: '/sms/signature/' + smsSignatureId,
    method: 'delete'
  })
}
