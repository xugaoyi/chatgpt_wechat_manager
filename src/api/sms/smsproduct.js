import request from '@/utils/request'

// 查询短信产品列表列表
export function listSmsproduct(query) {
  return request({
    url: '/sms/smsproduct/list',
    method: 'get',
    params: query
  })
}

// 查询短信产品列表详细
export function getSmsproduct(smsProductId) {
  return request({
    url: '/sms/smsproduct/' + smsProductId,
    method: 'get'
  })
}

// 新增短信产品列表
export function addSmsproduct(data) {
  return request({
    url: '/sms/smsproduct',
    method: 'post',
    data: data
  })
}

// 修改短信产品列表
export function updateSmsproduct(data) {
  return request({
    url: '/sms/smsproduct',
    method: 'put',
    data: data
  })
}

// 删除短信产品列表
export function delSmsproduct(smsProductId) {
  return request({
    url: '/sms/smsproduct/' + smsProductId,
    method: 'delete'
  })
}



// 删除短信产品列表
export function refreshAppkey(smsProductId) {
  return request({
    url: '/sms/smsproduct/refresh?smsProductId='+smsProductId,
    method: 'put'
  })
}


// 查询短信产品列表列表
export function listSmsproductsys(query) {
  return request({
    url: '/sms/smsproduct/sys/list',
    method: 'get',
    params: query
  })
}

// 查询短信产品列表详细
export function getSmsproductsys(smsProductId) {
  return request({
    url: '/sms/smsproduct/sys/' + smsProductId,
    method: 'get'
  })
}

// 新增短信产品列表
export function addSmsproductsys(data) {
  return request({
    url: '/sms/smsproduct/sys',
    method: 'post',
    data: data
  })
}

// 修改短信产品列表
export function updateSmsproductsys(data) {
  return request({
    url: '/sms/smsproduct/sys',
    method: 'put',
    data: data
  })
}

// 删除短信产品列表
export function delSmsproductsys(smsProductId) {
  return request({
    url: '/sms/smsproduct/sys/' + smsProductId,
    method: 'delete'
  })
}





