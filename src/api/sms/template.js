import request from '@/utils/request'

// 查询模板管理列表
export function listTemplate(query) {
  return request({
    url: '/sms/template/list',
    method: 'get',
    params: query
  })
}

// 查询模板管理详细
export function getTemplate(smsTemplateId) {
  return request({
    url: '/sms/template/' + smsTemplateId,
    method: 'get'
  })
}

// 新增模板管理
export function addTemplate(data) {
  return request({
    url: '/sms/template',
    method: 'post',
    data: data
  })
}

// 修改模板管理
export function updateTemplate(data) {
  return request({
    url: '/sms/template',
    method: 'put',
    data: data
  })
}

// 删除模板管理
export function delTemplate(smsTemplateId) {
  return request({
    url: '/sms/template/' + smsTemplateId,
    method: 'delete'
  })
}
