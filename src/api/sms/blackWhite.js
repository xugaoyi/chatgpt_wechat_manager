import request from '@/utils/request'

// 查询黑白名单列列表
export function listBlackWhite(query) {
  return request({
    url: '/sms/blackWhite/list',
    method: 'get',
    params: query
  })
}

// 查询黑白名单列详细
export function getBlackWhite(blackWhiteId) {
  return request({
    url: '/sms/blackWhite/' + blackWhiteId,
    method: 'get'
  })
}

// 新增黑白名单列
export function addBlackWhite(data) {
  return request({
    url: '/sms/blackWhite',
    method: 'post',
    data: data
  })
}

// 修改黑白名单列
export function updateBlackWhite(data) {
  return request({
    url: '/sms/blackWhite',
    method: 'put',
    data: data
  })
}

// 删除黑白名单列
export function delBlackWhite(blackWhiteId) {
  return request({
    url: '/sms/blackWhite/' + blackWhiteId,
    method: 'delete'
  })
}
